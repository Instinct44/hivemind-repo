package de.equilibrium.minddemoserver;

import java.util.ArrayList;
import java.util.List;

public class Player {
	
	private String name;
	private Connection connection;
	private List<Integer> cards;
	
	public Player(String name, Connection connection) {
		this.name = name;
		this.connection = connection;
		setCards(new ArrayList<Integer>());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public List<Integer> getCards() {
		return cards;
	}

	public void setCards(List<Integer> cards) {
		this.cards = cards;
	}

}
