package de.equilibrium.minddemoserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameManager {

	private static Player[] lobby2player;
	private static Player[] lobby3player;
	private static Player[] lobby4player;

	private static List<Lobby> activeLobbies;

	private static int lobbyId = 0;

	public static void init() {
		activeLobbies = new ArrayList<Lobby>();
		lobby2player = new Player[2];
		lobby3player = new Player[3];
		lobby4player = new Player[4];
	}

	public static void enqueue(int queue, String playerName, Connection connection) {
		Player newPlayer = new Player(playerName, connection);
		switch (queue) {
		case 2:
			if (lobby2player[0] == null) {
				lobby2player[0] = newPlayer;
			} else {
				lobby2player[1] = newPlayer;
				startGame(lobby2player);
				lobby2player = new Player[2];
			}
			break;
		case 3:
			break;
		case 4:
			break;
		}
	}

	public static void startGame(Player[] players) {
		String gameStartString = "!gamestart#" + lobbyId;
		Lobby lobby = new Lobby(lobbyId, players, 1);
		lobbyId++;
		for (Player player : players) {
			gameStartString += "#" + player.getName();
		}
		for (Player player : players) {
			player.getConnection().send(gameStartString);
		}
		activeLobbies.add(lobby);
		dealCards(lobby);
	}

	private static void dealCards(Lobby lobby) {
		List<Integer> cards = new ArrayList<Integer>();
		for (int i = 1; i <= lobby.getStage(); i++) {
			for (Player player : lobby.getPlayer()) {
				int card = new Random().nextInt(50) + 1;
				if (!cards.contains(card)) {
					cards.add(card);
					player.getCards().add(card);
				}
			}
		}
		for (Player player : lobby.getPlayer()) {
			String dealcardsString = "!dealcards#";
			for (int card : player.getCards()) {
				if (dealcardsString.split("#").length > 1) {
					dealcardsString += ",";
				}
				dealcardsString += card;
			}
			player.getConnection().send(dealcardsString);
		}
	}

	public static void playCard(int lobbyId, int playedCard) {
		for (Lobby lobby : activeLobbies) {
			if (lobby.getLobbyId() == lobbyId) {
				boolean isSmallest = true;
				for (Player player : lobby.getPlayer()) {
					int toRemoveCardIndex = -1;
					for (int i = 0; i < player.getCards().size(); i++) {
						if (player.getCards().get(i) < playedCard) {
							isSmallest = false;
						}
						if (player.getCards().get(i) == playedCard) {
							toRemoveCardIndex = i;
						}
					}
					if (toRemoveCardIndex >= 0) {
						player.getCards().remove(toRemoveCardIndex);
					}
				}
				if (isSmallest) {
					int totalCardCount = 0;
					for (Player player : lobby.getPlayer()) {
						totalCardCount += player.getCards().size();
					}
					if (totalCardCount == 0) {
						for (Player player : lobby.getPlayer()) {
							player.getConnection().send("!updatenumber#" + playedCard + "#won");
						}
					} else {
						for (Player player : lobby.getPlayer()) {
							player.getConnection().send("!updatenumber#" + playedCard + "#continue");
						}
					}
				} else {
					for (Player player : lobby.getPlayer()) {
						player.getConnection().send("!updatenumber#" + playedCard + "#lost");
					}
				}
			}
		}
	}

	public static void nextLevel(int lobbyId) {
		for (Lobby lobby : activeLobbies) {
			if (lobby.getLobbyId() == lobbyId) {
				lobby.setNextlevelconfirmed(lobby.getNextlevelconfirmed() + 1);
				if (lobby.getNextlevelconfirmed() == lobby.getPlayer().length) {
					lobby.setStage(lobby.getStage() + 1);
				}
			}
		}
	}

}
