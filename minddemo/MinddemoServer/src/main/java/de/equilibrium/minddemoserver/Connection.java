package de.equilibrium.minddemoserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Connection implements Runnable {
	
	private Socket clientSocket;
	
	private PrintWriter out;
	
	public Connection(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}

	@Override
	public void run() {
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String inputLine;
			while (true) {
				inputLine = in.readLine();
				System.out.println("input: " + inputLine);
				if(inputLine.startsWith("!register")) {
					GameManager.enqueue(Integer.parseInt(inputLine.split("#")[1]), inputLine.split("#")[2], this);
				}
				if(inputLine.startsWith("!played")) {
					GameManager.playCard(Integer.parseInt(inputLine.split("#")[1]), Integer.parseInt(inputLine.split("#")[2]));
				}
				if(inputLine.startsWith("!nextstage")) {
					GameManager.nextLevel(Integer.parseInt(inputLine.split("#")[1]));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void send(String message) {
		out.println(message);
		out.flush();
	}

}
