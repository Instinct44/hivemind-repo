package de.equilibrium.minddemoserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServerApplication {

	private static int port = 44651;
	public static boolean running = true;
	private static List<Connection> connectionList;

	public static void main(String[] args) throws IOException {
		GameManager.init();
		connectionList = new ArrayList<Connection>();
		ServerSocket serverSocket = new ServerSocket(port);
		System.out.println("started");
		while (running) {
			Socket clientSocket = serverSocket.accept();
			System.out.println("new connection");
			Connection connection = new Connection(clientSocket);
			connectionList.add(connection);
			new Thread(new Connection(clientSocket)).start();
		}
		System.out.println("stopped");
		serverSocket.close();
	}

}
