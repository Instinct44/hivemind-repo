package de.equilibrium.minddemoserver;

public class Lobby {

	private int lobbyId;
	private Player[] player;
	private int stage;
	private int nextlevelconfirmed;

	public Lobby(int lobbyId, Player[] player, int stage) {
		this.lobbyId = lobbyId;
		this.player = player;
		this.stage = stage;
	}

	public int getLobbyId() {
		return lobbyId;
	}

	public void setLobbyId(int lobbyId) {
		this.lobbyId = lobbyId;
	}

	public Player[] getPlayer() {
		return player;
	}

	public void setPlayer(Player[] player) {
		this.player = player;
	}

	public int getStage() {
		return stage;
	}

	public void setStage(int stage) {
		this.stage = stage;
	}

	public int getNextlevelconfirmed() {
		return nextlevelconfirmed;
	}

	public void setNextlevelconfirmed(int nextlevelconfirmed) {
		this.nextlevelconfirmed = nextlevelconfirmed;
	}

}
