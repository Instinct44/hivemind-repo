package de.equilibrium.minddemo;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FrameManager {

	private static int width = 400;
	private static int height = 400;
	private static int displayWidth = Toolkit.getDefaultToolkit().getScreenSize().width;
	private static int displayHeight = Toolkit.getDefaultToolkit().getScreenSize().height;

	private static JFrame frame;

	public static void startScreen() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("Hivemind Demo v0.0.1");
		frame.setSize(width, height);
		frame.setLocation(displayWidth / 2 - width / 2, displayHeight / 2 - height / 2);
		JPanel panel = new JPanel();
		panel.setLayout(null);

		JLabel labelTitle = new JLabel();
		labelTitle.setText("Hivemind Demo");
		labelTitle.setFont(new Font("Serif", Font.PLAIN, 20));
		labelTitle.setSize(150, 30);
		labelTitle.setLocation(125, 0);
		panel.add(labelTitle);

		JLabel labelInsertName = new JLabel();
		labelInsertName.setText("First enter username:");
		labelInsertName.setFont(new Font("Serif", Font.PLAIN, 14));
		labelInsertName.setSize(200, 40);
		labelInsertName.setLocation(0, 150);
		panel.add(labelInsertName);

		JTextField userNameField = new JTextField();
		userNameField.setSize(200, 40);
		userNameField.setLocation(200, 150);
		panel.add(userNameField);

		JLabel labelQueuetype = new JLabel();
		labelQueuetype.setText("Then select number of players:");
		labelQueuetype.setFont(new Font("Serif", Font.PLAIN, 14));
		labelQueuetype.setSize(200, 40);
		labelQueuetype.setLocation(0, 200);
		panel.add(labelQueuetype);

		JLabel queueUpdate = new JLabel();
		queueUpdate.setText("entered queue, please wait...");
		queueUpdate.setFont(new Font("Serif", Font.PLAIN, 16));
		queueUpdate.setSize(200, 20);
		queueUpdate.setLocation(100, 340);
		queueUpdate.setVisible(false);
		panel.add(queueUpdate);

		JButton buttonQueue2 = new JButton("2");
		buttonQueue2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!userNameField.getText().contentEquals("")) {
					ConnectionManager.send("!register#2#" + userNameField.getText());
					queueUpdate.setVisible(true);
					panel.repaint();
				}
			}
		});
		buttonQueue2.setSize(60, 40);
		buttonQueue2.setLocation(200, 200);
		panel.add(buttonQueue2);

		JButton buttonQueue3 = new JButton("3");
		buttonQueue3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!userNameField.getText().contentEquals("")) {
					ConnectionManager.send("!register#3#" + userNameField.getText());
					queueUpdate.setVisible(true);
					panel.repaint();
				}
			}
		});
		buttonQueue3.setSize(60, 40);
		buttonQueue3.setLocation(260, 200);
		panel.add(buttonQueue3);

		JButton buttonQueue4 = new JButton("4");
		buttonQueue4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!userNameField.getText().contentEquals("")) {
					ConnectionManager.send("!register#4#" + userNameField.getText());
					queueUpdate.setVisible(true);
					panel.repaint();
				}
			}
		});
		buttonQueue4.setSize(60, 40);
		buttonQueue4.setLocation(320, 200);
		panel.add(buttonQueue4);

		frame.getContentPane().add(panel);
		frame.setVisible(true);
	}

	private static JLabel labelCurrentNumber;
	private static JLabel labelGameStatus;
	private static JPanel panel;
	private static JPanel buttonPanel;
	private static JButton nextLevelButton;
	private static int activeLobbyId;

	public static void startGame(String inputString) {
		Color[] colorList = { Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW };
		List<Player> playerList = new ArrayList<Player>();
		activeLobbyId = Integer.parseInt(inputString.split("#")[1]);
		for (int i = 2; i < inputString.split("#").length; i++) {
			playerList.add(new Player(inputString.split("#")[i], 1));
		}

		frame.getContentPane().removeAll();
		panel = new JPanel();
		panel.setLayout(null);

		buttonPanel = new JPanel();
		buttonPanel.setLayout(null);
		buttonPanel.setSize(400, 40);
		buttonPanel.setLocation(0, 300);
		panel.add(buttonPanel);

		JLabel labelTitle = new JLabel();
		labelTitle.setText("Current Number:");
		labelTitle.setFont(new Font("Serif", Font.PLAIN, 20));
		labelTitle.setSize(150, 30);
		labelTitle.setLocation(125, 0);
		panel.add(labelTitle);

		labelCurrentNumber = new JLabel();
		labelCurrentNumber.setText("0");
		labelCurrentNumber.setFont(new Font("Serif", Font.PLAIN, 40));
		labelCurrentNumber.setSize(50, 50);
		labelCurrentNumber.setLocation(175, 140);
		panel.add(labelCurrentNumber);
		
		labelGameStatus = new JLabel();
		labelGameStatus.setText("");
		labelGameStatus.setFont(new Font("Serif", Font.PLAIN, 25));
		labelGameStatus.setSize(100, 40);
		labelGameStatus.setLocation(125, 180);
		panel.add(labelGameStatus);

		for (int i = 0; i < playerList.size(); i++) {
			JLabel labelPlayerName = new JLabel();
			labelPlayerName.setText(playerList.get(i).getName());
			labelPlayerName.setFont(new Font("Serif", Font.PLAIN, 16));
			labelPlayerName.setSize(100, 20);
			labelPlayerName.setLocation(i * 100, 340);
			labelPlayerName.setForeground(colorList[i]);
			panel.add(labelPlayerName);
		}

		frame.getContentPane().add(panel);
		frame.revalidate();
	}

	public static void updateCurrentNumber(String currentNumber, String gameStatus) {
		if(gameStatus.contentEquals("won")) {
			labelGameStatus.setText("you won");
			nextLevelButton = new JButton("next stage ->");
			nextLevelButton.setSize(150, 30);
			nextLevelButton.setLocation(125, 220);
			nextLevelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ConnectionManager.send("!nextstage#" + activeLobbyId);
					nextLevelButton.setEnabled(false);
				}
			});
			panel.add(nextLevelButton);
			panel.revalidate();
			panel.repaint();
		}
		if(gameStatus.contentEquals("lost")) {
			labelGameStatus.setText("you lost");
		}
		labelCurrentNumber.setText(currentNumber);
		panel.repaint();
	}

	public static void dealCards(String inputString) {
		List<Integer> cards = new ArrayList<Integer>();
		for (String cardValueString : inputString.split(",")) {
			cards.add(Integer.parseInt(cardValueString));
		}
		for (int i = 0; i < cards.size(); i++) {
			JButton cardButton = new JButton(cards.get(i) + "");
			cardButton.setSize(60, 40);
			cardButton.setLocation(60 * i, 0);
			final int cardValue = cards.get(i);
			cardButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					ConnectionManager.send("!played#" + activeLobbyId + "#" + cardValue);
					cardButton.setEnabled(false);
				}
			});
			buttonPanel.add(cardButton);
			buttonPanel.revalidate();
			buttonPanel.repaint();
		}
	}

}
