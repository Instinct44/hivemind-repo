package de.equilibrium.minddemo;

import java.io.BufferedReader;
import java.io.IOException;

public class ConnectionListener implements Runnable {
	
	private BufferedReader in;
	
	public ConnectionListener(BufferedReader in) {
		this.in = in;
	}

	@Override
	public void run() {
		String inputLine;
		while (true) {
			try {
				inputLine = in.readLine();
				System.out.println("input: " + inputLine);
				if(inputLine.startsWith("!gamestart")) {
					FrameManager.startGame(inputLine);
				}
				if(inputLine.startsWith("!updatenumber")) {
					FrameManager.updateCurrentNumber(inputLine.split("#")[1], inputLine.split("#")[2]);
				}
				if(inputLine.startsWith("!dealcards")) {
					FrameManager.dealCards(inputLine.split("#")[1]);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
