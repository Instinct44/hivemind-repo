package de.equilibrium.minddemo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class ConnectionManager {
	
	private static PrintWriter out;
	
	public static void startConnection() {
		try {
			Socket clientSocket = new Socket(InetAddress.getByName("2.204.222.58"), 44651);
			out = new PrintWriter(clientSocket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			new Thread(new ConnectionListener(in)).start();
		} catch (Exception e2) {
			e2.printStackTrace();
		}
	}
	
	public static void send(String message) {
		if(out == null) {
			startConnection();
		}
		out.println(message);
		out.flush();
	}

}
